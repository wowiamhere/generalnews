# General News Angular2 Application  

**Angular2** *(2.2.3)* via Angular-cli/Webpack serving data picked up from newsapi.org.  

- 8 angular components to display different data.  
- **angular/http** in service to fetch data.  
- separate service to pass data between components.  
- custom coded all capabilities.
- **Router.navigate** to move around.  
+ **RxJs** *5.0.0-beta.12* capabilities to handle news searches available from any view.  


**Live Website**  
https://generalnews.herokuapp.com  

**Repo**  
https://bitbucket.org/wowiamhere/generalnews  

**Online Portfolio**  
http://ZenCodeMaster.com

