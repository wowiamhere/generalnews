import { NgModule }       from '@angular/core';
import { BrowserModule }   from '@angular/platform-browser';
import { HttpModule }     from '@angular/http';
import { FormsModule }    from '@angular/forms';

  //for development 
//import { InMemoryWebApiModule } from 'angular-in-memory-web-api';

  //services
//import { InMemoryDataService }   from './services/in-memory-data.service';
import { NewsService }          from './services/news.service';
import { NewsSearchResultsService } from './services/news-search-results.service';

  //routing
import { routing } from './app.routing';

  //components
import { AppComponent }           from './components/app.component';
import { NewsDashboardComponent } from './components/news-dashboard.component';
import { MainNewsComponent }      from './components/main-news.component';
import { SportsNewsComponent }    from './components/sports-news.component';
import { ArticlesComponent }      from './components/articles.component';
import { SearchNewsComponent }    from './components/search-news.component';
import { SearchNewsResultsComponent } from './components/search-news-results.component';
import { FinancialNewsComponent } from './components/financial-news.component';

@NgModule({

imports: [
  BrowserModule,
  HttpModule,
  FormsModule,
  routing
//  ,
//  InMemoryWebApiModule.forRoot( InMemoryDataService )
],
declarations: [
  AppComponent,
  NewsDashboardComponent,
  MainNewsComponent,
  SportsNewsComponent,
  ArticlesComponent,
  SearchNewsComponent,
  SearchNewsResultsComponent,
  FinancialNewsComponent
],
providers: [ 
  NewsService,
  NewsSearchResultsService
],
bootstrap: [ AppComponent ]

})

export class AppModule {}
