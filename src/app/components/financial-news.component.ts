// Initialy display article data from various sources from http://newsapi.org
// Has a child component that will display a specific title search
// 	to the same category as initialiy displayed (articles)

// ---------------angular library-------------------------
import { Component, OnInit } 	from '@angular/core';
import { Router }							from '@angular/router';
import { Response }						from "@angular/http";

//--------------rxjs----------------------------------------
import { Observable, Subject, Subscription }	from 'rxjs';

//------------------Services---------------------------
	// Service to fetch data
import { NewsService } from '../services/news.service';

//---------------includes------------------------------
	// Api data for http call
	// 	e.x., plucks the source array from the object in file
	//	and passes it to the service
import { ApiInfo } from '../includes/news-api-org-info';

//-------------------component decorator---------------------
@Component({
	selector: 'financial-news',
	templateUrl: '../templates/financial-news.component.html',
	styleUrls: [ '../css/site.css' ]
})

//--------------------component class--------------------------
export class FinancialNewsComponent {

//----------------api results observable setup -------------------------
		// -results from api come in as an Observable<Object>
		// -these will be concatenated to form an array
		// -this array will be passed on to the this.newsSubject, which
		//		will be projected using asObservable() to be displayed in 
		// 		component template
	private newsSubject: Subject<any> = new Subject<any>();
	private news: Observable<any> = this.newsSubject.asObservable();
	private resultsCache: Array<Object> = [];

		// -to catch and kill subscription on destroy
	private subscription: Subscription;

// -----------------------------------------------------------------
		// Injeciting service
	constructor(
		private newsService: NewsService
	) {}

// -----------------------------------------------------------------
		// - calls service for news results
		// - receives a response object casting to json() and pushes onto array
		// - push final array onto subject.asObservable for displaying
	ngOnInit(): void {

		this.subscription = this.newsService.getInitialNewsView( ApiInfo.financialSources )
			.subscribe( {
				next: result => { 
						this.resultsCache.push( result );
						this.newsSubject.next( this.resultsCache );
					} 
				,
				error: err => console.error("From Financial COMPONENT--->", err)
		} );

	}

		// destroy kill on eomponent destroy
	ngOnDestroy() {
		this.subscription.unsubscribe();
	}	

}
