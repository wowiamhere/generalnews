// Root of components, will display the options for getting
// 	different types of information from api displayed via
// 	it's child components.

// ---------------angular library-------------------------
import { Component, OnInit, OnDestroy, Input }	from '@angular/core';
import { Router } 				from '@angular/router';
import { Http, Response } from '@angular/http';

//--------------rxjs----------------------------------------
import { Observable, Subject, Subscription } from 'rxjs';

//--------------------Services-----------------------------
	// service for fetching data from api
import { NewsService } from '../services/news.service';

//---------------includes------------------------------
	// Api data for http call
	// 	e.x., plucks the source array from the object in file
	//	and passes it to the service
import { ApiInfo } from '../includes/news-api-org-info';

//------------------------- decorator-------------------
@Component({
	selector: 		'news-dashboard',
	templateUrl:	'../templates/news-dashboard.component.html',
	styleUrls: [ '../css/site.css' ]
})

//-------------------------component class-------------------
export class NewsDashboardComponent implements OnInit{

//----------------api results observable setup -------------------------
		// -results from api come in as an Observable<Object>
		// -these will be concatenated to form an array
		// -this array will be passed on to the this.newsSubject, which
		//		will be projected using asObservable() to be displayed in 
		// 		component template
	private newsSubject: Subject<any> = new Subject<any>();
	private news: Observable<any> = this.newsSubject.asObservable();
	private resultsCache: Array<Object> = [];

		// to catch subscription and cancel at ngOnDestroy
	private subscription: Subscription;

// -----------------------------------------------------------------
		// Injecting
	constructor(
		private newsService: NewsService
	) { }
	
// -----------------------------------------------------------------
		// - calls service for news results
		// - receives a response object casting to json() and pushes onto array
		// - push final array onto subject.asObservable for displaying
	ngOnInit(): void {

		this.subscription = this.newsService.getInitialNewsView( ApiInfo.newsSources )
			.subscribe( 
				{ 
					next: ( result ) => {
								this.resultsCache = this.resultsCache.concat(result);
								this.newsSubject.next(this.resultsCache);
 							},
					error: err => console.error("From Dashboard COMPONENT--->", err)
				} 
			);
	}

// ------------------------------------------------------------------
			// kill subscriptions when component destroys
		ngOnDestroy(){
			this.subscription.unsubscribe();
		}

}