/*
	- uses NewsSearchResultsService to observe (rxjs) for results 
	- when results available
			* concatenates results to array and once all results in
				 projects to observable to be displayed
*/

// ---------------angular library-------------------------
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Response } 					from '@angular/http';
import { ActivatedRoute, Params } from '@angular/router';

//--------------rxjs----------------------------------------
import { Observable, Subject, Subscription } from 'rxjs';

//------------------services------------------------------------
	// -used to wait for results from http request to come through
	//		from search-news.component.
	// -this displays those results.
	// -we get here (this) via Router.location from search-news.component
import { NewsSearchResultsService } from '../services/news-search-results.service';

//-------------------component decorator---------------------
@Component({
	selector: 'search-news-results',
	templateUrl: '../templates/search-news-results.component.html',
	styleUrls: [ "../css/site.css" ]
})

export class SearchNewsResultsComponent implements OnInit, OnDestroy {

		// -when search term in url changes a new search has occured
		//		and it's time to clear the array of previous results
	private currentSearchTerm: string = "";

//----------------service results observable setup -------------------------
		// -results come in from service 
		// -these will be concatenated to form an array
		// -this array will be passed on to the this.newsSubject, which
		//		will be projected using asObservable() to be displayed in 
		// 		component template
	resultsCache = new Subject<any>();
	newsSearchResults: Observable<any> = this.resultsCache.asObservable();
	concatResult: Array<Object> = [];

		// to catch subscription and cancel at ngOnDestroy
	private subscription: Subscription;

		// -placing ActivatedRoute.params into observance to check for changes
		// -everytime there is a change, the array of results is emptied to make
		//		room for new incoming search results		
	private newSearch: Observable<any> = this.route.params;

		// -when service fails to come back with any matches for query, display this
		// -will be cleaned up upon response with matches
	private noResult: string = "";

// -----------------------------------------------------------------
		// -injecting service to fetch results from observable
	constructor(
		private route: ActivatedRoute, 
		private newsSearchResultsService: NewsSearchResultsService
	) {}


		// -initializes component with data received from service
		//		as son as it's available
		// - data comes through a service via the search-news.component
	ngOnInit(): void{

			// -everytime there is a change in the ActivatedRoute.params (new search)
			//		the array of results is cleared to make way for new batch
		this.newSearch.subscribe({next: () => this.concatResult = []});

		this.subscription = this.newsSearchResultsService.searchResults
			.subscribe({
					// -this receives the filtered news results from the api
					// -service returns 1 Array of 1 object per query if there are not results 
					// - response (array of objects) is checked, 
					//		* each object checked for no results ( via this.takeOutEmptyObjects )
					//		* only objects with results pushed into observable
					// 		* if no results at all, pass empty array to view and
					//				set component class property this.noResults
				next: (searchResult) => this.removeEmptyResults( searchResult )
			});

	}

// ----------------------------------------------------
	ngOnDestroy(){
		this.subscription.unsubscribe();
	}

// -------------class helper functions---------------------------------------
	
	removeEmptyResults( searchResult ){
		let takeOutEmptyObjects: Array<Object> = this.filterEmpty( searchResult );

		if( takeOutEmptyObjects[0]["title"] != "No Results Found" ){
			this.concatResult = this.concatResult.concat( takeOutEmptyObjects );
			this.resultsCache.next( this.concatResult );
		}

		if( this.concatResult.length == 0 ){
		 	this.noResult = ": No Results Available";
			this.resultsCache.next( this.concatResult );
		}else
			this.noResult = "";		
	}

		// - takes an iterable of objects
		// - checks if it has a property 'title' and whether it's set
		// - if so, empty result ignored 
		// - if not, result is prepared in temp array for returning to caller
	filterEmpty(arr: any): Array<Object>{
		let tempArr: Array<Object> = [];
		
		arr.forEach( (obj: Object) => obj['title'] != "no results available" 
			? tempArr.push( obj )
			: 0);
		
		return tempArr.length != 0 ? tempArr : [{title: "No Results Found"}];
	}

}


