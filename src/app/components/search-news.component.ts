/*
	- component has an input box for user to enter comma separated
			words or just sentences.
	- search entry is cleaned up 
			* trimmed
			* individual words separated from phrases between commas
			* all search terms/phrases sorted into array (one per index)
	- search terms are sent to service along with source of api
			* passed to this component via @Input property from app.component
	- results from query are passed on to search-news-results.component
			via NewsSearchResultsService (via observables) to be displayed
	- once results are passed, Router.navigate() to display component
			with search terms as url parameters
*/


import { Component, OnInit, OnDestroy, Input, Output } from '@angular/core';
import { Router }		from '@angular/router';
import { Response, ResponseOptions }	from '@angular/http';

//------------------------ Services ------------------------

	// to fetch data from http://newsapi.org api
import { NewsService } from '../services/news.service';
	// -service takes result from http request for 
	//		requesting component to pick it up
import { NewsSearchResultsService } from '../services/news-search-results.service';

//------------------------------------------------
	// api finformation (key, url, news sources, etc)
import { ApiInfo } from '../includes/news-api-org-info';

//------------------------------------------------
	// rxjs library
import { Subject, Observable, Subscription } from 'rxjs';


@Component({
	selector: 'search-news',
	templateUrl: '../templates/search-news.component.html',
	styleUrls: [ '../css/site.css' ]
})


	// Searches a certain news source from http://newsapi.org
	// 	-search based on a user input (string) and a news source selection
	// 	-this component has a search bar and a set of results it displays
	//	-if no results, the initial news loaded remiains and a message of no results is displayed
export class SearchNewsComponent implements OnInit{

// -----------------class properties-----------------------------------------
		// -value is passed via parent component
		// -user clicks button in parent component and function sets
		//		parents property while passing value to child (this) component
		// 		to be used as arguments to function for a service call to http://newsapi.org
	@Input() private source: string;

		// -user enters terms in a search box within this components view (html)
		// 		each term is placed within a Rxjs subject
	private newsSearchTerms = new Subject<string>();

		// -user enters search term
		// -results from corresponding user choice of source (this.source)
		//		are filtered using the search term
	private searchTerm: string = "";

		// -class attibute to be set when user makes new query.
		// -will be checked in response view for changes.
		// -each time there is a new query, the query display array (all results from)
		// 		current querry will be deleted to make way for new query results
	private newQuery: string = "";
	
	private subscription: Subscription;

// ----------------------------------------------------------
		// injecting service to component
	constructor(
		private newsService: NewsService,
		private router: Router,
		private newsSearchResultsService: NewsSearchResultsService
	) {}

// ----------------------------------------------------------
		// -takes user input (subject)
		//		* comma separated word/words
		//		* creates array and takes out repeat terms
		// 		* array passed to service for searching through results							
		// -service returns api results filtered by source and search term
		// -subscribes and if there are results returned 
		//		* they are sent to a service (NewsSearchResultsService)
		//		* router.navigate to change url
		//		* results are pulled from search-news-results.component via service (NewsSearchResultsService)
	ngOnInit(): void{

		this.subscription = this.newsSearchTerms
		 	.debounceTime(500)
			.distinctUntilChanged()
			.switchMap( ( term: string ) =>  { 
						let arrayOfTerms = this.arraySearchTerms(term);
						this.newQuery = "Search For " + arrayOfTerms.join('-');
						return (this.newsService.searchNews( arrayOfTerms , ApiInfo[this.source] ) ); 
					}
				)
				.subscribe({
						next: r => {
						 	this.newsSearchResultsService.searchResultsIn( r );
							this.turnSearchViewOn();						
						}
				});			

	}

	ngOnDestroy(): void{
		this.subscription.unsubscribe();
	}
// -----------------------------------------------------------
		// -takes a seach term:
		// 		* splits at comma for multi term search
		//		* trims each resulting string in array
		//		* removes duplicates with filter and Object.hasOwnProperty
		//		* passes final array to search function
	private arraySearchTerms(term: string): Array<string>{

		let temp = {};

		return term.split(',')
						.map( each => each.trim() )
						.filter( term => temp.hasOwnProperty(term) 
							? false 
							: (temp[term] = true));
	}
// ----------------------------------------------------------
		// -once results are in, navigate to component that will pull
		// 		results from service and displaying them for user
	private turnSearchViewOn(): void {	
		this.router.navigate(['/news_search_results', this.newQuery])
	}

// ----------------------------------------------------------
		// as user types, adds input to subject for query of api
	private searchNews( term: string ): void{
		this.newsSearchTerms.next(term);
	}

// ----------------------------------------------------------
		// toggles inline class name via [style.display]="none|block"
		// 	to display search bar 
		//	based on wether this.source has been selected (clicked) by user
	private returnClass(): string{
		return !this.source ? "none" : "block" 
	}

}