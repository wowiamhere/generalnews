// -Main component in application.
// -<news-app> element displayed within[root]/src/index.html
// -sets this.source via user click and communicates this value
// 		to [root]/src/app/components/search-news.component where
//		it is received via @Input property

import { Component, Input } from '@angular/core';

@Component({
	selector: 'news-app',
	templateUrl: '../templates/app.component.html',
	styleUrls: [ '../css/site.css' ]
})

export class AppComponent{

	private title: string = 'News, Articles and Stuff';

	private source: string = "";

	setSource(source: string): void{
		this.source = source;
	}

}