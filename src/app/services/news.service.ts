import { Injectable } from '@angular/core';
import { Http, Headers, URLSearchParams, Response, RequestOptionsArgs} from '@angular/http';

	// api information (apikey, url, sources array)
import { ApiInfo } from '../includes/news-api-org-info';

import 'rxjs/add/operator/toPromise';

import { Observable } from 'rxjs';

	// NewsService class:

	// 1. getInitialNewsView(): Observable<any>
		// Maps this.newsSources to function that sets up
		// 	parameters and headers for an http request to the newsapi.org
		// Returns an observable ready for subscription which
		//	initiates http request returning an object to view within
		// 	the component using the service.

@Injectable()
export class NewsService {

// ---------------------------------------------------------- 
		// Headers for different calls within the service.
	headers: Headers = new Headers( { 
			'Accept': 'application/json',
			'Access-Control-Allow-Origin': '*' 
		});
	
		// http://newsapi.org for more information on api. 
		// 	news api with various source options (free)
	private topStoriesUrl: string = ApiInfo.topStoriesUrl;
	private newsApiOrgKey: string = ApiInfo.newsApiOrgKey;

		// -to set as response when there are no matches in search Array<Object>
		// - with same property as the object from api
	private noResultsAnswer = [{title: "No Results Found" }];

// ----------------------------------------------------------
		// Injecting Http capabilities
	constructor( private http: Http ) {}

// ----------------------------------------------------------
		// -takes a string set by user as source for api call
		// -returns results in an observable
		// -maps an http request to each of the sources selected by user
		//		rxjs .concatAll() and pass each to rxjs .map
		// -pass value to class function to further filter result
	getInitialNewsView( src: string[] ): Observable<any> {

			// 	observable that will be used to emit the data sources
			// 	and pass them as arguments to service call
			// Each response of the api call will be stored in an array
			// 	of objects within the component where the call to this service is made.
		let newsSources: Observable<string> = Observable.from( src );
	 
	 return newsSources
	 	.map( source => this.getN( source ) )
	 			.concatAll()
	 			.map( resultObject => this.handleInitResponse( resultObject ) );
	
	}

		// -takes a Response Object (angular/http)
		// -returns a plain javascript object with filtered results
		// -extracts array of objects from Response (angular/http) 
		// -plucks number of objects from array (default 7)
		//	 randomly to ensure each call doesn't retrieve the same results all the time
		// -if Response Object's array contains less than the default of objects plucked
		// 		return original array with all objects
	private handleInitResponse( responseObject: Response): Object{

		let articlesArray: Array<Object> = responseObject.json().articles;
		let source: string = responseObject.json().source;
		let maxNumberOfResults = 3;
		let min: number = 0;
		let max: number = articlesArray.length - 1;
		let random: number = 0;
		let previousIndexArray: Array<number> = [];

		let responseArray: Array<Object> = [];

		let returnObject: Object = new Object();

		if( articlesArray.length > maxNumberOfResults ){

			while( previousIndexArray.length < maxNumberOfResults ){
				
				random = this.getRandomIndex( min, max );

				if( !previousIndexArray.includes( random ) ){
					previousIndexArray.push( random );
					responseArray.push( articlesArray[random] );
				}

			} 

			return { source: source, articles: responseArray };

		}else{
			return responseObject.json();
		}

	}

// ----------------------------------------------------------
		// -takes an array of terms for searching against.
		// -takes a source (as button), clicked by user
		// -makes http request to api for results to be
		// 		sorted agains user input: term
		// -calls class private function to hand over the array of
		// 		objects from request to be filtered
	searchNews( searchArray: Array<string>, src: string[]): Observable<any> {

		let sources: Observable<string> = Observable.from( src );

		return sources
			.map( source => this.getN( source ) )
			.concatAll()
			.map( responseObject => this.filterResult( responseObject.json().articles, searchArray) )
			.catch( (err) => {
				console.log("Service Error--->", err);
					// -in case there is some kind of error
					// 		return as no answer available for query
				return Observable.of( this.noResultsAnswer );
			})
						 
	}

//---------------------------------------------------------
		// -takes an array of objects and a search string (both set by user)
		// -loops through the articleArray to find matching data according to 
		// 		each string in array searchTerms
		// -checks only if search term is 2 characters of bigger to avoid all results returned
		// -places all matching objects in a local array to be returned to map (rxjs)
	private filterResult(articlesArray: any, searchTerms: Array<string>): Array<Object> {

		let filteredArticles: Array<Object> = [];

			articlesArray.forEach( (article: Object) => {
						searchTerms.forEach( term => 	( term.length >= 2 ) 
							&&  article['description'].toLowerCase().includes( term ) 
									? filteredArticles.push( article ) 
									: 0
						)
			});

		if(filteredArticles.length > 0)
			return filteredArticles;
		else
			return this.noResultsAnswer;

	} 	

// ----------------------------------------------------------
/* PRIVATE FUNCTIONS TO AID IN BUILDING HTTP REQUEST TO SERVICE */

private getRandomIndex(min: number, max: number){
	return Math.round( Math.random() * (max - min) + min );
}

//---------------------------------------------------
	 	// Receives a string from an observable to be used as the source of 
	 	//	the api data.
	 	// Takes the observable from the http request,
	 	// 	maps it to a json object to be iterated through 
	 	// 	in the view of the component where the calling 
	 	// 	function this function is part of gets subscribed to.
	private getN( source: string ): Observable<any> {

			// Sets query string parameters (function definition below)
		let parms = this.setGetParams( source );
			
			// Set options for request (parameters, headers) (function definition below)
		let reqOps = this.reqOptions( parms );

			// Returns a Json object to be iterated
			//	at the view for component calling this service
		return this.http.get( this.topStoriesUrl, reqOps );

	}


// ----------------------------------------------------------
		// For error handling
		// 	prints error to browser console
	private handleError(error: any): Promise<any>{
		console.error("FromSERVICE:::---:::-->   ", error);
		return Promise.reject( error.message || error );
	}	

// ----------------------------------------------------------
		// Takes a string and adds it to the query string
		//	along with the api key 
		// Returns a URLSearchParams object (imported from @angular/http)
	private setGetParams( source: string): URLSearchParams{
		let parms: URLSearchParams = new URLSearchParams();

		parms.set( "source", source );
		parms.set( "apikey", this.newsApiOrgKey );

		return parms;
	}

// ----------------------------------------------------------
		// Takes a URLSearchParams Object (imported from angular library)
		// Builds a reqOps Object based on the interface from angular/http
		// Returns reqOps with headers and params ready for http request
	private reqOptions( parms: URLSearchParams ): RequestOptionsArgs {
		let reqOps = {
			headers: this.headers,
			search: parms
		};

		return reqOps;
	}	

}

