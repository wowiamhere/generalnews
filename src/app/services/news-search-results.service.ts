// -Service used between components to relay when news-search.service
//		has returned with data 
// -Service receives data as observable 
// -service relays data to newsSearchResults component for displaying

import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { Subject, Observable } from 'rxjs';

@Injectable()

export class NewsSearchResultsService{

		// -to push response received at SearchNews component
		//		to newsSearchResults for displaying
	private newsSearchResults = new Subject<Response>();

	searchResults = this.newsSearchResults.asObservable();

	searchResultsIn(view: Response): void{
		this.newsSearchResults.next(view)
	}
}
