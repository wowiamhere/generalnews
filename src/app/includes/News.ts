// Object for response from http://newsapi.org

export class articles{
	author: string;
	description: string;
	title: string;
	url: string;
	urlToImage: string;
	publishedAt: string;

	constructor(au: string, desc: string, tit: string, url: string, utoI: string, pub: string ){
		this.author = au;
		this.description = desc;
		this.title = tit;
		this.url = url;
		this.urlToImage = utoI;
		this.publishedAt = pub;
	}
}

export class News{
	status: string;
	source: string;
	sortBy: string;
	articles: articles;

	constructor(st: string, sr: string, srt: string, art: articles){
		this.status = st;
		this.source = sr;
		this.sortBy = srt;
		this.articles.author = art.author;
		this.articles.description = art.description;
		this.articles.title = art.title;
		this.articles.url = art.url;
		this.articles.urlToImage = art.urlToImage;
		this.articles.publishedAt = art.publishedAt;
	}
}
