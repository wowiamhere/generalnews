		

		// http://newsapi.org for more information on api. 
		// 	news api with various source options (free)
	export let ApiInfo = {
		topStoriesUrl: "https://newsapi.org/v1/articles",
		newsApiOrgKey: "e4e2aa62a883464a87547e8de4336f61",

			// News sources from http://newsapi.org (free)
	newsSources: [
		"associated-press", 
		"bbc-news",
		"cnbc",
		"cnn",
		"reuters",
		"the-new-york-times",
		"google-news",
		"reddit-r-all",
		"abc-news-au",
		"metro",
		"newsweek",
		"sky-news",
		"the-guardian-au",
		"the-guardian-uk",
		"the-hindu",
		"the-times-of-india",
		"the-telegraph",
		"usa-today",
		"the-washington-post"
	],
	sportSources: [
		"bbc-sport",
		"espn",
		"fox-sports",
		"nfl-news",
		"sky-sports-news",
		"talksport",
		"the-sport-bible",
		"football-italia",
		"sky-sports-news"
	],
	articleSources: [
		"buzzfeed",
		"daily-mail",
		"focus",
		"four-four-two",
		"handelsblatt",
		"independent",
		"mashable",
		"bild",
		"mirror",
		"national-geographic",
		"new-scientist",
		"new-york-magazine",
		"polygon",
		"the-huffington-post",
		"the-lad-bible",
		"the-wall-street-journal",
		"time"
	],
	financialSources: [
		"bloomberg",
		"business-insider",
		"business-insider-uk",
		"financial-times",
		"fortune",
		"the-economist"
	],
	technologySources: [
		"ars-technica",
		"engadget",
		"recode",
		"reddit-r-all",
		"3tn",
		"techcrunch",
		"techradar",
		"the-next-web",
		"wired-de"
	],
	societySources: [
		"entertainment-weekly",
		"mtv-news",
		"mtv-news-uk",
		"ign"
	]

};



