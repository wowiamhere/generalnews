// Routing for application
// Injected at ./app.module.ts

import { ModuleWithProviders } 	from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

	// components from [root]/src/app/components
import { AppComponent } 					from './components/app.component';
import { NewsDashboardComponent } from './components/news-dashboard.component';
import { MainNewsComponent }			from './components/main-news.component';
import { SportsNewsComponent }		from './components/sports-news.component';
import { ArticlesComponent } 			from './components/articles.component';
import { SearchNewsComponent } 		from './components/search-news.component';
import { SearchNewsResultsComponent } from './components/search-news-results.component'; 
import { FinancialNewsComponent } from './components/financial-news.component';

const appRoutes: Routes = [
	{
		path: '',
		redirectTo: '/news_dashboard',
		pathMatch: 'full'
	},
	{
		path:'news_dashboard',
		component: NewsDashboardComponent
	},
	{
		path:'main_news',
		component: MainNewsComponent
	},
	{
		path: 'sports_news',
		component: SportsNewsComponent
	},
	{
		path: 'articles',
		component: ArticlesComponent
	},
	{
		path: 'financial_headlines',
		component: FinancialNewsComponent
	},
	{
		path: 'news_search_results/:term',
		component: SearchNewsResultsComponent
	}
	
];

export const routing: ModuleWithProviders = RouterModule.forRoot( appRoutes );
