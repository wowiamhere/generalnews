import { GeneralNewsPage } from './app.po';

describe('general-news App', function() {
  let page: GeneralNewsPage;

  beforeEach(() => {
    page = new GeneralNewsPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
